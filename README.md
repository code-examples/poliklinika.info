# [poliklinika.info](https://poliklinika.info) source codes

<br/>

### Run poliklinika.info on localhost

    # vi /etc/systemd/system/poliklinika.info.service

Insert code from poliklinika.info.service

    # systemctl enable poliklinika.info.service
    # systemctl start poliklinika.info.service
    # systemctl status poliklinika.info.service

http://localhost:4051
